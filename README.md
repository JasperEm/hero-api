# HERO API with [doctrine](https://github.com/doctrine/doctrine2) and [zend-expressive](https://github.com/zendframework/zend-expressive)

## Getting Started

```bash
$ composer update
```
Copy `doctrine.local.php.dist` to `doctrine.local.php` and add your database configuration.
Create the database schema with:

```bash
$ ./vendor/bin/doctrine orm:schema-tool:create 
```
and run the api:
```bash
$ composer run --timeout=0 serve
```

You can access the api: http://localhost:8080/api/hero