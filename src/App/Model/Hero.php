<?php
/**
 * Created by PhpStorm.
 * User: jasper
 * Date: 17.05.17
 * Time: 00:19
 *
 */

namespace App\Model;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Hero
 * @package App\Model
 *
 * @ORM\Entity(repositoryClass="App\Repository\HeroRepository")
 * @ORM\Table(
 *     name="hero",
 * )
 */
class Hero
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     *
     * @var int
     */
    protected $id;

    /**
     *
     * @ORM\Column(type="string")
     *
     * @var string
     */
    protected $name;

    /**
     * @ORM\OneToMany(targetEntity="Skill", mappedBy="hero")
     *
     * @var Skill
     */
    protected $skills;

    /**
     * Hero constructor.
     */
    public function __construct()
    {
        $this->skills = new ArrayCollection();
    }

    public function toArray()
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'skills_url' => 'hero/' . $this->getId() . '/skill',
        ];
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Hero
     */
    public function setId(int $id): Hero
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Hero
     */
    public function setName(string $name): Hero
    {
        $this->name = $name;
        return $this;
    }
}
