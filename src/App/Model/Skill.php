<?php
/**
 * Created by PhpStorm.
 * User: jasper
 * Date: 18.05.17
 * Time: 22:07
 */

namespace App\Model;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Hero
 * @package App\Model
 *
 * @ORM\Entity(repositoryClass="App\Repository\SkillRepository")
 * @ORM\Table(
 *     name="skill",
 * )
 */
class Skill
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     *
     * @var int
     */
    protected $id;

    /**
     *
     * @ORM\Column(type="string")
     *
     * @var string
     */
    protected $name;

    /**
     * @ORM\ManyToOne(targetEntity="Hero", inversedBy="skills")
     * @ORM\JoinColumn(name="hero_id", referencedColumnName="id")
     *
     * @var Hero
     */
    protected $hero;

    public function toArray(): array
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'hero_url' => 'hero/' . $this->getHero()->getId()
        ];
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Skill
     */
    public function setId(int $id): Skill
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Skill
     */
    public function setName(string $name): Skill
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return Hero
     */
    public function getHero(): Hero
    {
        return $this->hero;
    }

    /**
     * @param Hero $hero
     * @return Skill
     */
    public function setHero(Hero $hero): Skill
    {
        $this->hero = $hero;
        return $this;
    }
}
