<?php

namespace App\Repository;

use App\Model\Hero;
use Doctrine\ORM\EntityRepository;

class HeroRepository extends EntityRepository
{
    /**
     * @param null $name
     * @return Hero[]
     */
    public function findByName($name = null)
    {
        if (strlen($name)) {
            $qb = $this->createQueryBuilder('h')
                ->where('LOWER(h.name) like :name')
            ->setParameter('name', '%' . $name . '%');
            $result = $qb->getQuery()->getResult();
        } else {
            $result = $this->findAll();
        }

        return $result;
    }
}
