<?php

namespace App\Repository;

use App\Model\Skill;
use Doctrine\ORM\EntityRepository;

class SkillRepository extends EntityRepository
{
    /**
     * @param null $heroId
     * @return Skill[]
     */
    public function getAll($heroId = null)
    {
        if ($heroId) {
            $qb = $this->createQueryBuilder('s')
                ->where('s.hero = :hero')
            ->setParameter('hero', $heroId);
            $result = $qb->getQuery()->getResult();
        } else {
            $result = $this->findAll();
        }

        return $result;
    }
}
