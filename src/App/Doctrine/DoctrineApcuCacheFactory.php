<?php

namespace App\Doctrine;

use Doctrine\Common\Cache\ApcuCache;
use Interop\Container\ContainerInterface;

class DoctrineApcuCacheFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $config = $container->has('config') ? $container->get('config') : [];
        $cache = new ApcuCache();
        if (isset($config['doctrine']['cache']['namespace'])) {
            $cache->setNamespace($config['doctrine']['cache']['namespace']);
        }
        return $cache;
    }
}
