<?php

namespace App\Traits;

use Doctrine\ORM\PersistentCollection;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Traits UrlTrait
 * @package App\Traits
 */
trait UrlTrait
{
    /**
     * @param ServerRequestInterface $request
     * @param iterable $array $array
     * @return array
     */
    public function addCurrentUrl(ServerRequestInterface $request, iterable $array): array
    {
        foreach ($array as $key => $value) {
            if (!is_array($array[$key]) && !$array[$key] instanceof PersistentCollection) {
                if (strpos($key, '_url') !== false
                    && !$array[$key] instanceof PersistentCollection
                ) {
                    $array[$key] = $this->getCurrentUrlFromRequest($request)
                        . '/' . $array[$key];
                }
            } else {
                $array[$key] = $this->addCurrentUrl($request, $array[$key]);
            }
        }

        return $array;
    }

/**
 * @param ServerRequestInterface $request
 * @return string
 */
    protected function getCurrentUrlFromRequest(ServerRequestInterface $request)
    {
        $port = $request->getUri()->getPort();
        $url = $request->getUri()->getScheme()
        . '://' . $request->getUri()->getHost()
        . (in_array($port, [80, 443]) || !$port ? '' : ":$port");
        return $url;
    }
}
