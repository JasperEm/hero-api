<?php

namespace App\Action\Factory;

use App\Action\HeroAction;
use App\Model\Hero;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;

class HeroActionFactory
{
    public function __invoke(ContainerInterface $container): HeroAction
    {
        $heroAction = new HeroAction();
        /** @var EntityManager $em */
        $em = $container->get(EntityManager::class);

        $heroAction->setEntityManager($em);
        $heroAction->setHeroRepository($em->getRepository(Hero::class));

        return $heroAction;
    }
}
