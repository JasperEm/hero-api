<?php

namespace App\Action\Factory;

use App\Action\SkillAction;
use App\Model\Skill;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;

class SkillActionFactory
{
    public function __invoke(ContainerInterface $container): SkillAction
    {
        $skillAction = new SkillAction();
        /** @var EntityManager $em */
        $em = $container->get(EntityManager::class);

        $skillAction->setEntityManager($em);
        $skillAction->setEntityRepository($em->getRepository(Skill::class));

        return $skillAction;
    }
}
