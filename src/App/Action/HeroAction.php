<?php

namespace App\Action;

use App\Model\Hero;
use App\Repository\HeroRepository;
use App\Traits\UrlTrait;
use Doctrine\ORM\EntityManager;
use Fig\Http\Message\StatusCodeInterface;
use Interop\Http\ServerMiddleware\DelegateInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\JsonResponse;

class HeroAction extends AbstractAction
{
    use UrlTrait;

    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * @var HeroRepository
     */
    protected $heroRepository;

    /**
     * @param ServerRequestInterface $request
     * @param DelegateInterface $delegate
     * @return JsonResponse
     */
    public function get(ServerRequestInterface $request, DelegateInterface $delegate): JsonResponse
    {
        $id = $request->getAttribute('id');
        $name = isset($request->getQueryParams()['name']) ? $request->getQueryParams()['name']: null;
        $result = null;
        $statusCode = StatusCodeInterface::STATUS_OK;

        if ($id) {
            $result = $this->getHeroRepository()->find($id);
            if (!$result) {
                 $statusCode = StatusCodeInterface::STATUS_NOT_FOUND;
            }
        } elseif (strlen($name)) {
            $result = $this->getHeroRepository()->findByName($name);
        } else {
            $result = $this->getHeroRepository()->findAll();
        }
        $json = [];
        if ($result instanceof Hero) {
            $json = $this->addCurrentUrl($request, $result->toArray());
        } elseif (is_iterable($result)) {
            /** @var Hero $hero */
            foreach ($result as $hero) {
                $json[] = $this->addCurrentUrl($request, $hero->toArray());
            }
        }


        return new JsonResponse($json, $statusCode);
    }

    /**
     * @param ServerRequestInterface $request
     * @param DelegateInterface $delegate
     * @return JsonResponse
     */
    public function post(ServerRequestInterface $request, DelegateInterface $delegate): JsonResponse
    {
        $body = json_decode($request->getBody()->getContents(), true);

        if (isset($body['name'])) {
            $hero = new Hero();
            $hero->setName($body['name']);
            $this->getEntityManager()->persist($hero);
            $this->getEntityManager()->flush();

            return new JsonResponse(
                $this->addCurrentUrl($request, $hero->toArray()),
                StatusCodeInterface::STATUS_CREATED
            );
        }

        return new JsonResponse(['message' => 'INVALID CONTENT'], StatusCodeInterface::STATUS_NOT_ACCEPTABLE);
    }

    /**
     * @param ServerRequestInterface $request
     * @param DelegateInterface $delegate
     * @return JsonResponse
     */
    public function patch(ServerRequestInterface $request, DelegateInterface $delegate): JsonResponse
    {
        $body = json_decode($request->getBody()->getContents(), true);
        $id = $request->getAttribute('id');
        $hero = null;

        if (isset($body['name']) && $id) {
            $hero = $this->getHeroRepository()->find($id);
            if ($hero instanceof Hero) {
                $hero->setName($body['name']);
                $this->getEntityManager()->persist($hero);
                $this->getEntityManager()->flush();

                return new JsonResponse(
                    $this->addCurrentUrl($request, $hero->toArray()),
                    StatusCodeInterface::STATUS_ACCEPTED
                );
            }
        }

        return new JsonResponse(['message' => 'INVALID CONTENT'], StatusCodeInterface::STATUS_NOT_ACCEPTABLE);
    }

    /**
     * @param ServerRequestInterface $request
     * @param DelegateInterface $delegate
     * @return JsonResponse
     */
    public function put(ServerRequestInterface $request, DelegateInterface $delegate): JsonResponse
    {
        $body = json_decode($request->getBody()->getContents(), true);
        $id = $request->getAttribute('id');
        $hero = null;

        if (isset($body['name']) && $id) {
            $hero = $this->getHeroRepository()->find($id);
            if ($hero instanceof Hero) {
                $hero->setName($body['name']);
                $this->getEntityManager()->persist($hero);
                $this->getEntityManager()->flush();

                return new JsonResponse(
                    $this->addCurrentUrl($request, $hero->toArray()),
                    StatusCodeInterface::STATUS_ACCEPTED
                );
            }
        }

        return new JsonResponse(['message' => 'INVALID CONTENT'], StatusCodeInterface::STATUS_NOT_ACCEPTABLE);
    }

    /**
     * @param ServerRequestInterface $request
     * @param DelegateInterface $delegate
     * @return JsonResponse
     */
    public function delete(ServerRequestInterface $request, DelegateInterface $delegate): JsonResponse
    {
        $id = $request->getAttribute('id');
        if ($id) {
            $hero = $this->getHeroRepository()->find($id);
            if ($hero instanceof Hero) {
                $this->getEntityManager()->remove($hero);
                $this->getEntityManager()->flush();

                return new JsonResponse(['message' => 'Hero with id ' . $id . ' removed']);
            }
        }

        return new JsonResponse(['message' => 'INVALID CONTENT'], StatusCodeInterface::STATUS_NOT_ACCEPTABLE);
    }

    /**
     * @return EntityManager
     */
    public function getEntityManager(): EntityManager
    {
        return $this->entityManager;
    }

    /**
     * @param EntityManager $entityManager
     * @return HeroAction
     */
    public function setEntityManager(EntityManager $entityManager): HeroAction
    {
        $this->entityManager = $entityManager;
        return $this;
    }

    /**
     * @return HeroRepository
     */
    public function getHeroRepository(): HeroRepository
    {
        return $this->heroRepository;
    }

    /**
     * @param HeroRepository $heroRepository
     * @return HeroAction
     */
    public function setHeroRepository(HeroRepository $heroRepository): HeroAction
    {
        $this->heroRepository = $heroRepository;
        return $this;
    }
}
