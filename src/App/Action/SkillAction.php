<?php

namespace App\Action;

use App\Model\Hero;
use App\Model\Skill;
use App\Repository\SkillRepository;
use App\Traits\UrlTrait;
use Doctrine\ORM\EntityManager;
use Fig\Http\Message\StatusCodeInterface;
use Interop\Http\ServerMiddleware\DelegateInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\JsonResponse;

class SkillAction extends AbstractAction
{
    use UrlTrait;

    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * @var SkillRepository
     */
    protected $entityRepository;

    /**
     * @param ServerRequestInterface $request
     * @param DelegateInterface $delegate
     * @return JsonResponse
     */
    public function get(ServerRequestInterface $request, DelegateInterface $delegate): JsonResponse
    {
        $id = $request->getAttribute('id');
        $hero = $request->getAttribute('hero');
        $result = null;
        $statusCode = StatusCodeInterface::STATUS_OK;

        if ($id) {
            $result = $this->getEntityRepository()->find($id);
            if (!$result) {
                $statusCode = StatusCodeInterface::STATUS_NOT_FOUND;
            }
        } else {
            $result = $this->getEntityRepository()->getAll($hero);
        }

        $json = [];
        if ($result instanceof Skill) {
            $json = $this->addCurrentUrl($request, $result->toArray());
        } elseif (is_iterable($result)) {
            /** @var Skill $skill */
            foreach ($result as $skill) {
                $json[] = $this->addCurrentUrl($request, $skill->toArray());
            }
        }


        return new JsonResponse($json, $statusCode);
    }

    /**
     * @param ServerRequestInterface $request
     * @param DelegateInterface $delegate
     * @return JsonResponse
     */
    public function post(ServerRequestInterface $request, DelegateInterface $delegate): JsonResponse
    {
        $body = json_decode($request->getBody()->getContents(), true);

        if (isset($body['name']) && isset($body['hero'])) {
            $skill = new Skill();
            $skill->setName($body['name']);

            $hero = $this->getEntityManager()->getRepository(Hero::class)->find($body['hero']);
            if ($hero instanceof Hero) {
                $skill->setHero($hero);
                $this->getEntityManager()->persist($skill);
                $this->getEntityManager()->flush();

                return new JsonResponse(
                    $this->addCurrentUrl($request, $skill->toArray()),
                    StatusCodeInterface::STATUS_CREATED
                );
            }
        }

        return new JsonResponse(['message' => 'INVALID CONTENT'], StatusCodeInterface::STATUS_NOT_ACCEPTABLE);
    }

    /**
     * @param ServerRequestInterface $request
     * @param DelegateInterface $delegate
     * @return JsonResponse
     */
    public function patch(ServerRequestInterface $request, DelegateInterface $delegate): JsonResponse
    {
        $body = json_decode($request->getBody()->getContents(), true);
        $id = $request->getAttribute('id');
        $skill = null;

        if ((isset($body['name']) || isset($body['hero'])) && $id) {
            $skill = $this->getEntityRepository()->find($id);

            if ($skill instanceof Skill) {
                if ($body['hero']) {
                    $hero = $this->getEntityManager()->getRepository(Hero::class)->find($body['hero']);
                    if ($hero instanceof Hero) {
                        $skill->setHero($hero);
                    }
                }

                if ($body['name']) {
                    $skill->setName($body['name']);
                }

                $this->getEntityManager()->persist($skill);
                $this->getEntityManager()->flush();
                return new JsonResponse(
                    $this->addCurrentUrl($request, $skill->toArray()),
                    StatusCodeInterface::STATUS_ACCEPTED
                );
            }
        }

        return new JsonResponse(['message' => 'INVALID CONTENT'], StatusCodeInterface::STATUS_NOT_ACCEPTABLE);
    }

    /**
     * @param ServerRequestInterface $request
     * @param DelegateInterface $delegate
     * @return JsonResponse
     */
    public function put(ServerRequestInterface $request, DelegateInterface $delegate): JsonResponse
    {
        $body = json_decode($request->getBody()->getContents(), true);
        $id = $request->getAttribute('id');
        $skill = null;

        if (isset($body['name']) && isset($body['hero']) && $id) {
            $skill = $this->getEntityRepository()->find($id);
            if ($skill instanceof Skill) {
                $skill->setName($body['name']);
                $hero = $this->getEntityManager()->getRepository(Hero::class)->find($body['hero']);

                if ($hero instanceof Hero) {
                    $skill->setHero($hero);
                    $this->getEntityManager()->persist($skill);
                    $this->getEntityManager()->flush();
                    return new JsonResponse(
                        $this->addCurrentUrl($request, $skill->toArray()),
                        StatusCodeInterface::STATUS_ACCEPTED
                    );
                }
            }
        }

        return new JsonResponse(['message' => 'INVALID CONTENT'], StatusCodeInterface::STATUS_NOT_ACCEPTABLE);
    }

    /**
     * @param ServerRequestInterface $request
     * @param DelegateInterface $delegate
     * @return JsonResponse
     */
    public function delete(ServerRequestInterface $request, DelegateInterface $delegate): JsonResponse
    {
        $id = $request->getAttribute('id');
        if ($id) {
            $skill = $this->getEntityRepository()->find($id);
            if ($skill instanceof Skill) {
                $this->getEntityManager()->remove($skill);
                $this->getEntityManager()->flush();

                return new JsonResponse(['message' => 'Skill with id ' . $id . ' removed']);
            }
        }

        return new JsonResponse(['message' => 'INVALID CONTENT'], StatusCodeInterface::STATUS_NOT_ACCEPTABLE);
    }

    /**
     * @return EntityManager
     */
    public function getEntityManager(): EntityManager
    {
        return $this->entityManager;
    }

    /**
     * @param EntityManager $entityManager
     * @return SkillAction
     */
    public function setEntityManager(EntityManager $entityManager): SkillAction
    {
        $this->entityManager = $entityManager;
        return $this;
    }

    /**
     * @return SkillRepository
     */
    public function getEntityRepository(): SkillRepository
    {
        return $this->entityRepository;
    }

    /**
     * @param SkillRepository $entityRepository
     * @return SkillAction
     */
    public function setEntityRepository(SkillRepository $entityRepository): SkillAction
    {
        $this->entityRepository = $entityRepository;
        return $this;
    }
}
