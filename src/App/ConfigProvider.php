<?php

namespace App;

use App\Action\Factory\SkillActionFactory;
use App\Action\HeroAction;
use App\Action\Factory\HeroActionFactory;
use App\Action\RootAction;
use App\Action\SkillAction;
use App\Doctrine\DoctrineApcuCacheFactory;
use App\Doctrine\DoctrineFactory;
use App\Middleware\CorsMiddleware;
use Doctrine\Common\Cache\Cache;
use Doctrine\ORM\EntityManager;

/**
 * The configuration provider for the App module
 *
 * @see https://docs.zendframework.com/zend-component-installer/
 */
class ConfigProvider
{
    /**
     * Returns the configuration array
     *
     * To add a bit of a structure, each section is defined in a separate
     * method which returns an array with its configuration.
     *
     * @return array
     */
    public function __invoke()
    {
        return [
            'dependencies' => $this->getDependencies(),
            'templates'    => $this->getTemplates(),
        ];
    }

    /**
     * Returns the container dependencies
     *
     * @return array
     */
    public function getDependencies()
    {
        return [
            'invokables' => [
                RootAction::class => RootAction::class,
                CorsMiddleware::class => CorsMiddleware::class,
            ],
            'factories'  => [
                HeroAction::class => HeroActionFactory::class,
                SkillAction::class => SkillActionFactory::class,
                EntityManager::class => DoctrineFactory::class,
                Cache::class => DoctrineApcuCacheFactory::class,
            ],
        ];
    }

    /**
     * Returns the templates configuration
     *
     * @return array
     */
    public function getTemplates()
    {
        return [
            'paths' => [
                'app'    => ['templates/app'],
                'error'  => ['templates/error'],
                'layout' => ['templates/layout'],
            ],
        ];
    }
}
