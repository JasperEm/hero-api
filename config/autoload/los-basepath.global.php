<?php

return [
    'dependencies' => [
        'factories' => [
            LosMiddleware\BasePath\BasePath::class => LosMiddleware\BasePath\BasePathFactory::class,
        ],
    ],

    'middleware_pipeline' => [
        'always' => [
            'middleware' => [
                LosMiddleware\BasePath\BasePath::class
            ],
        ],
    ],

];
