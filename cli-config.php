<?php
use Doctrine\Common\Annotations\AnnotationRegistry;
use Doctrine\ORM\Tools\Setup;

require 'vendor/autoload.php';
$path = ['src/App/Model'];
$devMode = true;
AnnotationRegistry::registerLoader('class_exists');
$config = Setup::createAnnotationMetadataConfiguration($path, $devMode, null, null, false);

$applicationConfig = require_once __DIR__ . '/config/config.php';

$connectionOptions = [
    'driver' => $applicationConfig['doctrine']['connection']['orm_default']['driver'],
    'host' => $applicationConfig['doctrine']['connection']['orm_default']['host'],
    'port' => $applicationConfig['doctrine']['connection']['orm_default']['port'],
    'user' => $applicationConfig['doctrine']['connection']['orm_default']['user'],
    'password' => $applicationConfig['doctrine']['connection']['orm_default']['password'],
    'dbname' => $applicationConfig['doctrine']['connection']['orm_default']['dbname'],
];



$em = \Doctrine\ORM\EntityManager::create($connectionOptions, $config);

$helpers = new Symfony\Component\Console\Helper\HelperSet(array(
    'db' => new \Doctrine\DBAL\Tools\Console\Helper\ConnectionHelper($em->getConnection()),
    'em' => new \Doctrine\ORM\Tools\Console\Helper\EntityManagerHelper($em)
));
